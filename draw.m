
%import list of files
dir_list = dir(fullfile('Data'));
size_dir_list = size(dir_list);
size_dir_list = size_dir_list(1);
file_list = cell(size_dir_list,1);

for i=3:size_dir_list
    file_list{i} = dir_list(i).name;
    i = i+1;
end

%start reading data for each bus
%bus34_WPP678.dbECU24.csv
previous_bus_name = char('');

for i_file_list=3:size_dir_list
    
    % find the name of the file
    file_name = char(file_list(i_file_list));
    [file_name_sub,remain] = strtok(file_name,'.');
    [file_name_section2,remain] = strtok(remain,'.');
    
    if not(strcmp(previous_bus_name,file_name_sub ))
        
        %find day of the file
        if (strfind(file_name_section2,'ECU') == 3 )
            file_name_day = strtok(file_name_section2,'dbECU');
        else if (strfind(file_name_section2,'GPS') == 3 )
                file_name_day = strtok(file_name_section2,'dbGPS');
            else if (strfind(file_name_section2,'STATUS') == 3 )
                    file_name_day = strtok(file_name_section2,'dbSTATUS');
                end
            end
        end
        
        %import data for this bus
        file_name_ECU = strcat('Data/',file_name_sub,'.dbECU',file_name_day,'.csv');
        file_name_GPS = strcat('Data/',file_name_sub,'.dbGPS',file_name_day,'.csv');
        file_name_STATUS = strcat('Data/',file_name_sub,'.dbSTATUS',file_name_day,'.csv');
        
        status = importdata(file_name_STATUS,',',1);
        status = status.data;
        
        if exist(file_name_ECU , 'file')
            ecu = importdata(file_name_ECU,',',1);
            ecu = ecu.data;
        else
            ecu = 0;
        end
        
        if exist(file_name_GPS , 'file')
            [NUM,TXT,RAW] = xlsread(file_name_GPS);
            
            gps = NUM;
        else
            gps = 0;
        end
        
        
        % calculate the params and graphs
        
        %cut out matrixes out of big one
        status_size = size(status);
        status_size = status_size(1);
        gps_size = size(gps);
        gps_size = gps_size(1);
        ecu_size = size(ecu);
        ecu_size = ecu_size(1);
        if (ecu_size == 1)
            flag_ecu_data =0;
        else
            flag_ecu_data=1;
        end
        
        
        time_matrix = double(status(:,2:4));
        time_status = (( time_matrix(:,3))/60 + time_matrix(:,2))/60 + time_matrix(:,1);
        
        % remove faulty time 23:59:59
        
        while(time_status(1) > 23)
            time_status(1,:) =[];
            status(1,:) =[];
            gps(1,:) =[];
            status_size = status_size-1;
            gps_size = gps_size - 1;
            
        end
        
        if (flag_ecu_data)
            time_matrix = double(ecu(:,2:5));
            time_ecu = ((time_matrix(:,4)/1000 + time_matrix(:,3))/60 + time_matrix(:,2))/60 + time_matrix(:,1);
            
            % remove faulty time 23:59:59
            while(time_ecu(1) > 23)
                time_ecu(1,:) =[];
                ecu(1,:) =[];
                ecu_size = ecu_size -1;
            end
            
            speed = double(ecu(:,6));
            rpm = double(ecu(:,7));
            fppn = double(ecu(:,8));
            ect = double(ecu(:,9));
        end
        
        % calculating the period that engine is on

        engine = status(:,6) + status(:,7);
        j=1;
        while (j<status_size)
            if engine(j) == 2
                engine(j) =1;
            else
                engine(j)=0;
            end
            j = j+1;
        end
        
        %creating ecu time-based matrix for engine on
        

        i=1;
        j=1;
        engine_ecu = double([]);
        
        while(i<=status_size)
            
            while(j<=ecu_size && time_ecu(j)<time_status(i))
                engine_ecu(j,1) = engine(i);
                j=j+1;
            end
            i = i+1;
        end
        %fill in the blanks
        while(j<=ecu_size)
            engine_ecu(j,1) = engine(status_size);
            j = j+1;
        end
        
        
        i=1;
        j=1;
        total_duration_engine_on = 0;
        
        while (j<status_size)
            
            while (engine(j) == 0 && j<status_size)
                j = j+1;
            end
            
            if (j==status_size)
                break;
            end
            
            time_0 = time_status(j);
            
            while (engine(j) == 1 && j<status_size)
                j = j+1;
            end
            
            if (j==status_size && engine(j)==0)
                break;
            end
            
            time_1 = time_status(j);
            total_duration_engine_on = total_duration_engine_on + ( time_1 - time_0);
            time_0 = 0;
            
        end
        
        if (flag_ecu_data)
            
            %detecting if speed is wrong and switch to read speed from gps
            i=1;
            j=1;
            
            speed_source=0;

                  %calculate the gps speed
            time_matrix = double(gps(:,2:4));
            time_gps = (time_matrix(:,3)/60 + time_matrix(:,2))/60 + time_matrix(:,1);
            i=1;
            j=1;
            speed_gps = double([]);
            
            while(i<=gps_size)
                
                while(j<=ecu_size && time_ecu(j)<time_gps(i))
                    speed_gps(j,1) = gps(i,14);
                    j=j+1;
                end
                i = i+1;
            end
            %fill the blank speed fields
            while(j<=ecu_size)
                speed_gps(j,1) = 0;
                j = j+1;
            end
            
            %claculate corrolation between speeds
            i=1;
            similarity = 0;
            while(i<=ecu_size)
                
                similarity = similarity + ( speed(i)-speed_gps(i) )^2;
                i = i+1;
            end
            
            similarity = sqrt(similarity)/mean(speed);
                
            if (similarity>100)
                % switch to read speed from gps
                speed_source=1;
                
                speed = speed_gps;
            end
                
            
            
            
            %filtering the speed spikes
            i=1;
            j=1;
            moving = int16([]);
            
            while (i<ecu_size)
                
                if (speed(i) > 0)
                    moving(j)= speed(i);
                    if (i~=1)
                        if ( abs(speed(i)-speed(i-1)) > 50)
                            speed(i)=speed(i-1);
                        end
                    end
                    j = j+1;
                end
                
                i=i+1;
            end
            
            speed_max = max(speed);
            speed_ave_while_moving = mean(moving);
            
            %break (idle) time calculator
            time_0=0;
            break_time=0;
            short_stop_time= 0;
            i=1;
            state_flow_new = 0;
            state_flow_old = 2;
            
            while (i<ecu_size)
                
                
                if ( speed(i) > 0 && engine_ecu(i)==1)
                    state_flow_new = 0;
                else if ( speed(i) ==0 && rpm(i) >0 && engine_ecu(i) ==1)
                        state_flow_new = 1;
                    else if ( engine_ecu(i) == 0 ) %speed(i) == 0 && rpm(i) ==0 )
                            state_flow_new = 2;
                        end
                    end
                end
                
                if (state_flow_new == 1)
                    
                    if (state_flow_old ~= 1 )
                        time_0 = time_ecu(i);
                    end
                
                else
                    
                    if (state_flow_old == 1)
                        step_idle_time = time_ecu(i) - time_0;
                        if (step_idle_time > 0.33 )
                            break_time = break_time + step_idle_time; 
                        else
                            short_stop_time = short_stop_time + step_idle_time;
                        end
                    end
                    
                end
                
                state_flow_old = state_flow_new;
                i = i +1;
            end
                
            
            idling_wastage=break_time*3.88;          
            
            
            
            %standard_deviation = std(speed)/ave_speed_while_moving;
            
            
            %filtering the ect spikes
            i=2;
            while (i<ecu_size)
                
                if (ect(i) > 100)
                    ect(i)=ect(i-1);
                end
                
                i=i+1;
            end
            
            ect_max = max(ect);
            ect_ave = mean(ect);
            
            % duration that the throttle is pressed but bus is stationary
            i=1;
            j=0;
            while (i<ecu_size)
                if (speed(i) == 0)
                    if (fppn(i) >0)
                        
                        j = j+1;
                    end
                end
                i=i+1;
            end
            
            zero_speed_foot_pedal_pressed = j/4/60;
            
            %Ratio of stop time while engine is on to total time
            i=1;
            j=0;
            while (i<ecu_size)
                if (speed(i) == 0 && rpm(i)>0)
                    j = j+1;
                end
                i=i+1;
            end
            
            ratio_stoptime_to_totaltime = j/i;
            
            %Over speeding
            i=1;
            j=1;
            speeding = double([]);
            while (i<ecu_size)
                if (speed(i) > 70)
                    speeding(j)= speed(i);
                    j = j+1;
                end
                i=i+1;
            end
            
            
            ratio_Overspeed = j/i;
            
            coeff = ones(1, 50)/20;
            avg_fppn = filter(coeff, 1, fppn);
            
            
            %mileage
            i=2;
            mileage_ecu=0;
            
            while (i<ecu_size)
                
                delta_time = time_ecu(i)-time_ecu(i-1);
                mileage_ecu = mileage_ecu + speed(i-1)*delta_time;
                i = i+1;
                
            end
            
            i=2;
            mileage_gps=0;
            r = 6371; %km
            
            while (i<gps_size)
                
                % x= r*teta z^2=x^2+y^2
                z =sqrt( ( ( gps(i,9)-gps(i-1,9)) * pi *r / 180 )^2 + ( ( gps(i,10)-gps(i-1,10) )  * pi *r / 180)^2 );
                mileage_gps = mileage_gps + z;
                i = i+1;
                
            end
            
        end%    if (flag_ecu_data)
        
        %put the params into file
        foldername_out = strcat('Output_',file_name_day);
        mkdir(foldername_out);
        filename_out = strcat(foldername_out,'/',file_name_sub);
        A ={'total_duration_engine_on','ratio_stoptime_to_totaltime','speed_max','speed_ave_while_moving','Ratio_Overspeed','mileage_ecu','mileage_gps','ect_max','ect_ave','zero_speed_foot_pedal_pressed','break_time','idling_wastage','short_stop_time','speed_source'};
        sheet = 1;
        %xlswrite(filename_out,A,sheet);
        B = double(zeros(size(A)));
        B(1) = total_duration_engine_on;
        
        if (flag_ecu_data)
            
            B(2) = ratio_stoptime_to_totaltime*100;
            B(3) = speed_max;
            B(4) = speed_ave_while_moving;
            B(5) = ratio_Overspeed*100;
            B(6) = mileage_ecu;
            B(7) = mileage_gps;
            B(8) = ect_max;
            B(9) = ect_ave;
            B(10)= zero_speed_foot_pedal_pressed;
            B(11)= break_time;
            B(12)= idling_wastage;
            B(13)= short_stop_time;
            B(14)= speed_source;
        end
        %xlRange = 'A2';
        %xlswrite(filename_out,B,sheet,xlRange);
        %save filename_out A;
        csvwrite (filename_out,cell2mat(A));
        
        fig=figure();
        plot(time_status,engine,'-r');
        title(strcat('Engine Status of :',file_name_sub));
        xlabel('Time : Hour');
        ylabel('Engine');
        axis ([round(time_status(1)) round(time_status(status_size)) -0.1 1.1]);
        a = gca;
        set (a,'YTick',[0 1]);
        set (a,'YTickLabel',{'OFF','ON'})
        set (a, 'XTick',[round(time_status(1)):round(time_status(status_size))]);
        saveas(fig,strcat(filename_out,'engine'),'pdf');
        
        if (flag_ecu_data)
            x_axis_limits = [round(time_ecu(1))-0.5 round(time_ecu(ecu_size))+0.5];
            x_axis_ticks = [round(time_ecu(1)):round(time_ecu(ecu_size))];
            
            fig=figure();
            plot(time_ecu,speed,'-b');
            title(strcat('Speed of :',file_name_sub));
            xlabel('Time : Hour');
            ylabel('Speed: Km/h');
            axis ([x_axis_limits 0 speed_max+10]);
            a = gca;
            set (a,'YTick',[0:10:speed_max+10]);
            set (a, 'XTick',x_axis_ticks);
            saveas(fig,strcat(filename_out,'speed'),'pdf');
            
            fig=figure();
            plot(time_ecu,rpm,'-r');
            title(strcat('RPM of :',file_name_sub));
            xlabel('Time : Hour');
            ylabel('RPM');
            axis ([x_axis_limits 0 max(rpm)+100]);
            a = gca;
            set (a,'YTick',[0:200:max(rpm)+100]);
            set (a, 'XTick',x_axis_ticks);
            saveas(fig,strcat(filename_out,'rpm'),'pdf');
            
            fig=figure();
            plot(time_ecu,fppn,'-g');
            title(strcat('FPPn of :',file_name_sub));
            xlabel('Time : Hour');
            ylabel('FPPn %');
            axis ([x_axis_limits 0 100]);
            a = gca;
            set (a,'YTick',[0:20:100]);
            set (a, 'XTick',x_axis_ticks);
            saveas(fig,strcat(filename_out,'fppn'),'pdf');
            
            fig=figure();
            plot(time_ecu,ect,'-b');
            title(strcat('ECT of :',file_name_sub));
            xlabel('Time : Hour');
            ylabel('ECT: C');
            axis ([x_axis_limits 25 ect_max+5]);
            a = gca;
            set (a,'YTick',[25:5:ect_max+5]);
            set (a, 'XTick',x_axis_ticks);
            saveas(fig,strcat(filename_out,'ect'),'pdf');
        end
        
        %remember which bus was read to avoid duplicates
        previous_bus_name = file_name_sub;
        close all;
    end
end %end of main for

